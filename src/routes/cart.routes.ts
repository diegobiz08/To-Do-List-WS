import {Router} from 'express';
import {createCart, deleteProductFromCart, getCartDetails, updateCart} from '../controllers/cart.controller';
import passport from "passport";

const router = Router();

router.post('/api/carrito', passport.authenticate('jwt', {session: false}), createCart);
router.put('/api/carrito/:id', passport.authenticate('jwt', {session: false}), updateCart);
router.delete('/api/carrito', passport.authenticate('jwt', {session: false}), deleteProductFromCart);
router.post('/api/carrito/byId', passport.authenticate('jwt', {session: false}), getCartDetails);

export default router;
